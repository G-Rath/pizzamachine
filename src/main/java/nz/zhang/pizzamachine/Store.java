package nz.zhang.pizzamachine;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.Timer;

/**
 * Store
 * <p/>
 * Created by G-Rath on 27/03/2015.
 */
public class Store extends JFrame
{
	private final static Store INSTANCE = new Store();

	private final static String defaultPizzaFileText = "garlic-and-olives: { garlic: 3, olives: 2 }\r\n" +
			                                                   "olives-and-garlic: { garlic: 2, olives: 3 }\r\n" +
			                                                   "the-works: { garlic: 1, olives: 4, mushrooms: 5, anchovies: 5 }\r\n" +
			                                                   "anchovies-and-mushrooms: { anchovies: 5, mushrooms: 5 }\r\n" +
			                                                   "meatlovers: { mushrooms: 10 }\r\n" +
			                                                   "fishy-olives: { olives: 5, anchovies: 5 }";

	public static Font monoFont = new Font( "Monospaced", Font.BOLD, 15 );

	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenX = 640;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenY = 640;

	private JLayeredPane lpane = new JLayeredPane();
	/** Global timer that makes the program tick */
	@SuppressWarnings( "FieldCanBeLocal" )
	private Timer tickTimer;
	/** Reference to the pizza machine */
	private PizzaMachine machine;
	/** Reference to the TextPrinter which prints text to the JFrame in a list */
	private TextPrinter printer;
	private LoadingScreen loadingScreen;

	private HashSet<String> menu = new HashSet<>();

	private ArrayList<BufferedImage> containerImages = new ArrayList<>();
	private ArrayList<BufferedImage> customerImages = new ArrayList<>();
	private HashMap<String, ArrayList<BufferedImage>> supplierSprites = new HashMap<String, ArrayList<BufferedImage>>();

	private boolean useAlternativeHeli = true;

	private boolean imagesLoaded_Customer = false;
	private boolean imagesLoaded_Container = false;
	private boolean imagesLoaded_Supplier = false;

	public Store()
	{
		setJFrameWindowIcon(); //Set the JFrame window icon here otherwise it will show with the Java icon for a secnod or two
	}

	public static Store getInstance()
	{
		return INSTANCE;
	}

	/**
	 * scales an image using AffineTransform by the given x/y factors
	 *
	 * @param sbi       image to scale
	 * @param imageType type of image
	 * @param dWidth    width of destination image
	 * @param dHeight   height of destination image
	 * @param fWidth    x-factor for transformation / scaling
	 * @param fHeight   y-factor for transformation / scaling
	 *
	 * @return scaled image
	 */
	public static BufferedImage scale( BufferedImage sbi, int imageType, int dWidth, int dHeight, double fWidth, double fHeight )
	{
		BufferedImage dbi = null;
		if( sbi != null )
		{
			dbi = new BufferedImage( (int) ( dWidth * fWidth ), (int) ( dHeight * fHeight ), imageType );
			Graphics2D g = dbi.createGraphics();
			AffineTransform at = AffineTransform.getScaleInstance( fWidth, fHeight );
			g.drawRenderedImage( sbi, at );
		}
		return dbi;
	}

	/** Set up the global tick timer that runs in this class to tick the whole program every 100ms */
	public void setupTimer()
	{
		tickTimer = new Timer( true );
		tickTimer.schedule( new TimerTask()
		{
			@Override
			public void run()
			{
				Store.getInstance().tick();
			}
		}, 100, 100 );
	}

	public void finishedResupply( Ingredient ingredient )
	{
		System.out.println( "finished resupplying" );
		machine.updateSupplyRequest( ingredient, -1 ); //state -1 means request completed
		machine.getIngredientContainer( ingredient ).setWaitingForResupply( false );
	}

	public void preSetupFrame()
	{
		setLayout( new BorderLayout() );

		this.getContentPane().setBackground( Color.BLUE );

		setTitle( "Pizza Store (c) Gareth Jones 2015" );

		lpane.setPreferredSize( new Dimension( 640, 640 ) );
		lpane.setBackground( Color.BLACK );
		lpane.setOpaque( true );

		add( lpane, BorderLayout.CENTER );

		loadingScreen = new LoadingScreen();
		loadingScreen.setOpaque( false );
		loadingScreen.setBounds( 0, 0, screenX, screenY );
		lpane.add( loadingScreen, new Integer( 1 ) );

		requestFocus();
		setResizable( true );
		setVisible( true );
		pack();
		setLocationRelativeTo( null );
	}

	public void setJFrameWindowIcon()
	{
		try
		{
			String filePath = "/sprites/pizza-icon.png";
			InputStream in = getClass().getResourceAsStream( filePath );
			BufferedImage image = ImageIO.read( in );

			setIconImage( image );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	public void setupStoreVisuals()
	{
		//If this is done with declaration the class seems to be "constructed" twice (At least, system prints are double-uped).
		//I suspect that something with Swing causes the class to be constructed again when added to lpane if its not "freshly constructed".
		//I don't know the conditions for Swing to consider the class "fresh" (vs "not-fresh") aside from being instanced in this function satisfies it.
		//More research is underway, and hopefully will turn up more - Howe ver since its working and GUI isn't the main focus of this program, its a low priority / non-issue.
		machine = new PizzaMachine( new Point( 200, 200 ) );
		//machine = new PizzaMachine( new Point( screenX / 2 - 100, screenY / 2 - 100 ), true );
		machine.setOpaque( false );
		machine.setBounds( 0, 0, screenX, screenY );

		printer = new TextPrinter( new Point( 20, screenY - 20 ) );
		printer.setOpaque( false );
		printer.setBounds( 0, 0, screenX, screenY );

		/**
		 * WARNING: you must use INTEGER with add for a layered-pane, not an int. The compiler WON'T complain if you use an int BUT it will result in a layer issue
		 * See the java doc "How to use a Layered Pane" for more - Gareth Jones
		 */
		lpane.add( printer, new Integer( 0 ) );
		lpane.add( machine, new Integer( 1 ) );

		this.getContentPane().setBackground( Color.BLACK );
		this.getContentPane().setForeground( Color.white );
		machine.setForeground( Color.white );

		requestFocus();
		setResizable( true );
		setVisible( true );
		pack();
		setLocationRelativeTo( null );
	}

	public void setupStoreCustomers()
	{
		Point customerLocation = new Point( 5, 300 );

		System.out.println( customerImages.get( 0 ).getWidth() );

		createNewCustomer( "Joey", customerLocation );
		customerLocation.x += customerImages.get( 0 ).getWidth() + 20;
		createNewCustomer( "Allen", customerLocation );
		customerLocation.x += customerImages.get( 0 ).getWidth() + 20;
		createNewCustomer( "Mikey", customerLocation );
		customerLocation.x += customerImages.get( 0 ).getWidth() + 20;
		createNewCustomer( "Cass", customerLocation );
	}

	public void setupWorkers()
	{
		loadContainerImagesWithWorker();
	}

	public void loadContainerImagesWithWorker()
	{
		SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>()
		{
			@Override
			protected Boolean doInBackground() throws Exception
			{
				Store.getInstance().loadContainerImages();
				return true;
			}

			@Override
			protected void done()
			{
				imagesLoaded_Container = true;
				doneLoadingImages();
				loadCustomerImagesWithWorker();
				super.done();
			}

			@Override
			protected void process( List<Void> chunks )
			{
				loadingScreen.repaint();
				super.process( chunks );
			}
		};

		worker.execute();
	}

	public void loadCustomerImagesWithWorker()
	{
		SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>()
		{
			@Override
			protected Boolean doInBackground() throws Exception
			{
				Store.getInstance().loadCustomerImages();
				return true;
			}

			@Override
			protected void done()
			{
				imagesLoaded_Customer = true;
				doneLoadingImages();
				loadSupplierImagesWithWorker();
				super.done();
			}

			@Override
			protected void process( List<Void> chunks )
			{
				loadingScreen.repaint();
				super.process( chunks );
			}
		};

		worker.execute();
	}

	public void loadSupplierImagesWithWorker()
	{
		SwingWorker<Void, String> worker = new SwingWorker<Void, String>()
		{
			@Override
			protected Void doInBackground() throws Exception
			{
				Store.getInstance().loadSupplierSprites();
				return null;
			}

			@Override
			protected void done()
			{
				imagesLoaded_Supplier = true;
				doneLoadingImages();
				super.done();
			}

			@Override
			protected void process( List<String> chunks )
			{
				loadingScreen.repaint();
				super.process( chunks );
			}
		};

		worker.execute();
	}

	public void doneLoadingImages()
	{
		if( imagesLoaded_Container && imagesLoaded_Supplier && imagesLoaded_Customer )
		{
			System.out.println( customerImages.size() );
			finishSettingUpStore();
		}
	}

	public void finishSettingUpStore()
	{
		setupStoreVisuals();
		readPizzasFromFile();
		setupStoreCustomers();
		setupTimer();

		lpane.remove( loadingScreen );
	}

	public void setupStore()
	{
		preSetupFrame();
		setupWorkers();
	}

	private void createNewCustomer( String name, Point location )
	{
		VisualCustomer vCustomer = new VisualCustomer( new Point( location ) );
		new Thread( new Customer( menu, name, 1, 1, vCustomer ) ).start();

		vCustomer.setOpaque( false );
		vCustomer.setBounds( 0, 0, screenX, screenY );
		lpane.add( vCustomer, new Integer( 1 ) );
		pack();
	}

	private void loadContainerImages()
	{
		System.out.println( "Loading: Container images" );

		for( int i = 0; i < 6 + 1; i++ )
		{
			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;

			try
			{
				String filePath = "/sprites/hpad/hpad " + imageNumberString + ".png";
				InputStream in = getClass().getResourceAsStream( filePath );
				BufferedImage image = ImageIO.read( in );

				containerImages.add( scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
			}
			catch( IOException e )
			{
				JOptionPane.showMessageDialog( null, "Error - Sprites not found for: Container" );
				e.printStackTrace();
			}
		}

		System.out.println( "\t" + "done" );
	}

	private void loadCustomerImages()
	{
		System.out.println( "Loading: Customer images" );

		for( int i = 0; i < 6 + 1; i++ )
		{
			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;

			try
			{
				String filePath = "/sprites/service-depot-ra/fix " + imageNumberString + ".png";
				InputStream in = getClass().getResourceAsStream( filePath );
				BufferedImage image = ImageIO.read( in );

				customerImages.add( scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
			}
			catch( IOException e )
			{
				JOptionPane.showMessageDialog( null, "Error - Sprites not found for: Customer" );
				e.printStackTrace();
			}
		}

		System.out.println( "\t" + "done" );
	}

	public void loadSupplierSprites()
	{
		System.out.println( "Loading: Supplier images" );

		ArrayList<BufferedImage> frames = new ArrayList<>();

		for( int i = 0; i < 31 + 1; i++ )
		{
			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;

			try
			{
				String filePath = "/sprites/orca-td/orca " + imageNumberString + ".png";
				InputStream in = getClass().getResourceAsStream( filePath );
				BufferedImage image = ImageIO.read( in );

				frames.add( scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
				System.out.println( ( i + 1 ) + " / 64" );
			}
			catch( IOException e )
			{
				JOptionPane.showMessageDialog( null, "Error - Sprites not found for: Supplier (Orca, landing)" );
				e.printStackTrace();
			}
		}

		supplierSprites.put( "orca-landing", frames );

		frames = new ArrayList<>();

		for( int i = 32; i < 63 + 1; i++ )
		{
			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;

			try
			{
				String filePath = "/sprites/orca-td/orca " + imageNumberString + ".png";
				InputStream in = getClass().getResourceAsStream( filePath );
				BufferedImage image = ImageIO.read( in );

				frames.add( scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
			}
			catch( IOException e )
			{
				JOptionPane.showMessageDialog( null, "Error - Sprites not found for: Supplier (Orca, flying)" );
				e.printStackTrace();
			}
		}

		supplierSprites.put( "orca-flying", frames );

		frames = new ArrayList<>();

		for( int i = 0; i < 31 + 1; i++ )
		{
			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;

			try
			{
				String filePath = "/sprites/heli/heli " + imageNumberString + ".png";
				InputStream in = getClass().getResourceAsStream( filePath );
				BufferedImage image = ImageIO.read( in );

				frames.add( scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
			}
			catch( IOException e )
			{
				JOptionPane.showMessageDialog( null, "Error - Sprites not found for: Supplier (Heli)" );
				e.printStackTrace();
			}
		}

		supplierSprites.put( "heli-landing", frames );
		supplierSprites.put( "heli-flying", frames );

		System.out.println( "\t" + "done" );
	}

	public ArrayList<BufferedImage> getContainerImages()
	{
		return containerImages;
	}

	public ArrayList<BufferedImage> getCustomerImages()
	{
		return customerImages;
	}

	public ArrayList<BufferedImage> getSupplierSpriteArray( String spriteArrayName )
	{
		return supplierSprites.get( spriteArrayName );
	}

	/**
	 * Add text to the TextPrinter to be displayed on the screen in the text list
	 *
	 * @param text text to add
	 */
	public void addTextToPrinter( String text )
	{
		printer.addText( text );
	}

	public void readPizzasFromFile()
	{
		try( BufferedReader reader = new BufferedReader( new FileReader( "pizza-list.txt" ) ) )
		{
			String line;

			while( ( line = reader.readLine() ) != null )
			{
				//If the line is a blank line (such as a left over new line at the end), or is a comment line ("#"), ignore it
				if( !line.equals( "" ) && !line.trim().startsWith( "#"  ) )
					menu.add( line ); //The above condition is the only error checking we do in this file as the rest would take too much time to write for this assignment.
			}
		}
		catch( IOException e )
		{
			if( e instanceof FileNotFoundException )
				createNewPizzaFile(); //If the exception is that we can't find the file, try and create the file from default hardcode and then try again.
			else
				e.printStackTrace();
		}
	}

	/**
	 * Called when the pizza menu file is not found to create a new version of the file using hardcoded defaults
	 */
	private void createNewPizzaFile()
	{
		try
		{
			FileWriter writer = new FileWriter( "pizza-list.txt" );
			writer.write( defaultPizzaFileText );
			writer.close();
			readPizzasFromFile();
		}
		catch( IOException e )
		{
			JOptionPane.showMessageDialog( null, "Error - Pizza menu file not found, and attempting to create file failed" );
			e.printStackTrace();
		}
	}

	/**
	 * Heart-beat of the program. Each "tick" represents one in-program second - A second relative for the program.
	 * Most instances that are on the main drawing thread and that have tick() are called from here, either directly (like PizzaMachine)
	 * or via waterfall from the instances primary owner (like with IngredientContainer) - this is because their owner is a better "ticker"
	 * than this class, normally because the owner is the primary owner and control the main reference and do most of the interaction to/with that instance
	 */
	private void tick()
	{
		checkForResupply();
		machine.tick();
	}

	public void checkForResupply()
	{
		machine.cleanSupplyRequests();

		HashMap<Ingredient, Integer> supplyRequestMap = new HashMap<>( machine.getSupplyRequestMap() );

		Iterator it = supplyRequestMap.entrySet().iterator();
		while( it.hasNext() )
		{
			Map.Entry pair = (Map.Entry) it.next();

			//request state 0 means the request has been made but not yet started
			if( (int) pair.getValue() == 0 )
			{
				requestResupply( machine.getIngredientContainer( (Ingredient) pair.getKey() ) );
				//Pairs are not by reference, so we have to call "getSupplyRequestMap" to get a reference to the actual HashMap.
				machine.getSupplyRequestMap().put( (Ingredient) pair.getKey(), 1 ); //supply state 1 means request has been seen
			}

			it.remove(); // avoids a ConcurrentModificationException
		}
	}

	/**
	 * Checks if the machine is currently free to take new orders, or if its busy
	 *
	 * @return true if its safe to place an order with the machine, false otherwise
	 */
	public boolean canPlaceOrder()
	{
		return machine.isSafeToPlaceOrder();
	}

	/**
	 * Places an order with the PizzaMachine for a pizza
	 *
	 * @param pizza the pizza order to be placed so that the PizzaMachine will make it later
	 */
	public void placeOrder( Pizza pizza )
	{
		machine.placeOrder( pizza );
	}

	/**
	 * Removes a VisualSupplier from the Store JFrame. Once removed the VisualSupplier will no longer be drawn and if no other references to it
	 * exist it will be orphaned and later collected by the GC. Removing the VisualSupplier from the JFrame is very important since it lives on the
	 * main Swing event thread and will always take up system memory and resources even if not actually visible on screen.
	 *
	 * @param vSupplier the VisualSupplier component to be removed
	 */
	public void removeVSupplier( VisualSupplier vSupplier )
	{
		System.out.println( "Removing visual supplier" );
		lpane.remove( vSupplier );
	}

	public void requestResupply( IngredientContainer containerToSupply )
	{
		System.out.println( "creating new supplier" );

		VisualSupplier vSupplier = new VisualSupplier();
		new Thread( new Supplier( vSupplier, containerToSupply, useAlternativeHeli ) ).start();

		useAlternativeHeli = !useAlternativeHeli; //Every second Supplier is to be the second type of Heli - The difference is only visual

		vSupplier.setOpaque( false );
		vSupplier.setBounds( 0, 0, screenX, screenY );

		lpane.add( vSupplier, new Integer( lpane.getComponentCount() + 1 ) );
		pack();
	}

	private class LoadingScreen extends JPanel
	{
		String loadingText = "loading";

		public void doDrawing( Graphics g )
		{
			g.setFont( Store.monoFont );

			Graphics2D g2d = (Graphics2D) g;

			g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
			g2d.setColor( Color.WHITE );

			Point stringCenter = new Point( getFontMetrics( getFont() ).stringWidth( loadingText ), getFontMetrics( getFont() ).getHeight() );
			g2d.drawString( loadingText, ( screenX / 2 ) - stringCenter.x, screenY / 2 );

			Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
			g2d.dispose();                      //Dispose the copy of the Graphic we made
		}

		@Override
		public void paintComponent( Graphics g )
		{
			super.paintComponent( g );
			doDrawing( g );
		}
	}
}