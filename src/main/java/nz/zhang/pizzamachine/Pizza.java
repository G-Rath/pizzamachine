package nz.zhang.pizzamachine;

import java.awt.*;
import java.util.HashMap;

/**
 * Pizza class - Stores information about a pizza, and is used both as an order and actual pizza.
 * <p/>
 * Created by G-Rath on 8/04/2015.
 */
public class Pizza
{
	/** HashMap of the ingredients that are in this pizza, and how many of those ingredients. */
	private HashMap<Ingredient, Integer> ingredients = new HashMap<>();
	/** The name of this pizza */
	private String pizzaName = "none";
	/** Has this pizza been made yet? */
	private boolean isMade = false;
	/** Has this pizza been made, and has other references to the pizza (mainly via PizzaMachine) been removed? Hence - is this pizza ready for eating? */
	private boolean hasBeenEaten = false;

	/** This is just to make output a little easier, by having the name we can do things like "pizza made for " + pizza.getOrderer - this should NEVER be used for conditions */
	private String orderer = "none";
	/** This is also just to make output a little easier - by having the location stored we can draw a line from the machine to the customer taking the order */
	private Point ordererLocation = null;

	@SuppressWarnings( "unused" )
	public Pizza( HashMap<Ingredient, Integer> ingredients )
	{
		this.ingredients = new HashMap<>( ingredients );
	}

	public Pizza( String pizzaOrder )
	{
		//Used just to shut IntelliJ up about setPizzaName being unused without using an annotator suppressor
		setPizzaName( pizzaOrder.substring( 0, pizzaOrder.indexOf( ':' ) ));

		//Substring at the brace start and end, but not the actual braces ( so "pizza: { olives: 2 }" becomes " olives: 2 "), and then trim off the leading + trailing spaces
		String ingredientsString = pizzaOrder.substring( pizzaOrder.indexOf( '{' ) + 1, pizzaOrder.lastIndexOf( '}' ) - 1 ).trim();

		//Check if the ingredientsString end's with a "," - This isn't a requirement, because we will add it if its not there, since its required for looping
		//Check if the ingredientsString ends with a "," - This isn't an user-requirement, but we want to remove it since it will mess up the loop if its there
		if( ingredientsString.endsWith( "," ) )
			ingredientsString = ingredientsString.substring( 0, ingredientsString.length() - 1 );

		System.out.println( pizzaOrder );
		//System.out.println( ingredientsString );

		String currentIngredientString;

		while( ingredientsString.indexOf( ',' ) != -1 )
		{
			currentIngredientString = ingredientsString.substring( 0, ingredientsString.indexOf( ',' ) );

			ingredientsString = ingredientsString.substring( ingredientsString.indexOf( ',' ) + 1 ).trim();
			parseAndAddIngredientFromString( currentIngredientString );

			System.out.println( "\t" + currentIngredientString );
		}

		//currentIngredientString lastly is the leftover from ingredientsString, which should have now only one ingredient data
		currentIngredientString = ingredientsString;

		parseAndAddIngredientFromString( ingredientsString );

		System.out.println( "\t" + currentIngredientString );
		System.out.println( "" );
	}

	@SuppressWarnings( "unused" )
	public Pizza( int[][] ingredientsArray )
	{
		//Cache the result of Ingredient.values() as Java arrays are considered mutable and so calling values() in the loop would require Java to make a new array for each call.
		Ingredient[] ingredientValues = Ingredient.values();

		for( int i = 0, ingredientsLength = ingredientsArray.length; i < ingredientsLength; i++ )
		{
			ingredients.put( ingredientValues[ingredientsArray[i][0]], ingredientsArray[i][1] );
		}
	}

	public void parseAndAddIngredientFromString( String line )
	{
		if( line.indexOf( ':' ) != -1 )
		{
			Ingredient ingredient = Ingredient.fromString( line.substring( 0, line.indexOf( ':' ) ) );

			//System.out.println( currentIngredientString.substring( ':' ).trim() );

			int ingredientCount = Integer.parseInt( line.substring( line.indexOf( ':' ) + 1 ).trim() );
			addIngredient( ingredient, ingredientCount );
		}

	}

	/**
	 * Adds an ingredient to the ingredient HashMap, that holds the ingredients (and their amounts) required to make this pizza.
	 * <p/>
	 * When adding an ingredient the HashMap is checked, so if the ingredient is already listed its requirement is added to, not overridden.
	 *
	 * @param ingredient - The ingredient to be required for this pizza
	 * @param amount     - The amount of the above ingredient to be required for this pizza
	 */
	public void addIngredient( Ingredient ingredient, int amount )
	{
		//Don't use containsKey since we end up putting into the HashMap either way
		if( ingredients.get( ingredient ) == null )
			ingredients.put( ingredient, amount );
		else //Else if a count is already in the HashMap, add to that amount
			ingredients.put( ingredient, amount + ingredients.get( ingredient ) );
	}

	/**
	 * Get the ingredients HashMap which contains the ingredients and their amounts required to make this pizza.
	 *
	 * @return - ingredients HashMap
	 */
	public HashMap<Ingredient, Integer> getIngredients()
	{
		return ingredients;
	}

	@SuppressWarnings( "unused" )
	public boolean isMade()
	{
		return isMade;
	}

	public void setIsMade( boolean isMade )
	{
		this.isMade = isMade;
	}

	public boolean hasBeenEaten()
	{
		return hasBeenEaten;
	}

	public void setHasBeenEaten( boolean hasBeenEaten )
	{
		this.hasBeenEaten = hasBeenEaten;
	}

	public String getPizzaName()
	{
		return pizzaName;
	}

	public void setPizzaName( String pizzaName )
	{
		this.pizzaName = pizzaName;
	}

	public String getOrderer()
	{
		return orderer;
	}

	public void setOrderer( String orderer )
	{
		this.orderer = orderer;
	}

	public Point getOrdererLocation()
	{
		return ordererLocation;
	}

	public void setOrdererLocation( Point ordererLocation )
	{
		this.ordererLocation = ordererLocation;
	}

//	public static HashMap<Ingredient, Integer> createIngredientsHashMap( ArrayList<Ingredient> ingredients, ArrayList<Integer> amounts )
//	{
//		HashMap<Ingredient, Integer> returnMap = new HashMap<>();
//
//		for( int i = 0, ingredientsSize = ingredients.size(); i < ingredientsSize; i++ )
//		{
//			Ingredient ingredient = ingredients.get( i );
//
//		}
//	}
}
