package nz.zhang.pizzamachine;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * {@code Customer} class - orders {@code Pizza}s
 * <p/>
 * Created by G-Rath on 8/04/2015.
 */
public class Customer implements Runnable
{
	/** The most hungry this customer can be */
	private static final int MAX_HUNGER = 200;

	@SuppressWarnings( "unused" )
	private final int waitTime = 10;

	private ArrayList<BufferedImage> imageFrames = new ArrayList<>();

	/**
	 * Represents how hunger this {@code Customer} is - the greater this number, the greater the chance of them ordering a pizza.
	 * Ranges between 0 and MAX_HUNGER
	 */
	private int hunger_level = 0;
	/**
	 * Represents how quickly this {@code Customer} gets hungry.
	 * This impacts on how often this customer will place an order
	 */
	private int hungerBuildupRate = 1;
	/** Represents how satisfied this {@code Customer} hunger levels are when they eat a {@code Pizza} */
	private float hunger_modifier = 1;

	private int currentFrame = 0;

	/** Reference variable so we don't have to keep calling getImage().getWidth() */
	private int imgWidth = 0;
	/** Reference variable so we don't have to keep calling getImage().getHeight() */
	private int imgHeight = 0;

	@SuppressWarnings( "unused" )
	private int waitingCounter = 0;

	/** ArrayList of the pizza menu - what pizzas the machine makes and hence that the customer can order */
	private ArrayList<String> menu = new ArrayList<>();
	/** Current pizza this customer has ordered and is waiting on. If null then the customer hasn't placed an order */
	private Pizza currentOrder = null;
	/** The name of this {@code Customer} - used mainly for console output */
	private String name;

	private VisualCustomer vCustomer;

	public Customer( HashSet<String> menu, String name, int hungerBuildupRate, float hunger_modifier, VisualCustomer vCustomer )
	{
		getImagesFromStore();

		this.menu = new ArrayList<>( menu ); //Create a new ArrayList using the menu given - By using a set in Store each element must be unique, but now we can "get" as an array
		this.name = name;
		this.hungerBuildupRate = hungerBuildupRate;
		this.hunger_modifier = hunger_modifier;
		this.vCustomer = vCustomer;

		vCustomer.setCustomerName( name );
	}

	/** Gets the center point of this {@code Customer}, calculated by using the width and height of the {@code Customer} image */
	private Point getCenterLocation()
	{
		return new Point( vCustomer.getDrawLoc().x + ( imgWidth / 2 ), vCustomer.getDrawLoc().y + ( imgHeight / 2 ) );
	}

	public void placePizzaOrder()
	{
		int ranNum = new Random().nextInt( menu.size() );
		Pizza order = new Pizza( menu.get( ranNum ) );
		order.setOrdererLocation( getCenterLocation() );

		order.setOrderer( name );
		Store.getInstance().placeOrder( order );

		System.out.println( name + ": Placing new order: " + order.getPizzaName() );

		currentOrder = order;
	}

	/**
	 * Get animation images from the {@code Store}'s global array of said images. This is done to keep the heapstack's size down.
	 * This way we only have to read the actual images from file once, regardless of how many {@code Customer}s that are instanced.
	 */
	private void getImagesFromStore()
	{
		imageFrames = Store.getInstance().getCustomerImages();

		imgWidth = imageFrames.get( 0 ).getWidth();
		imgHeight = imageFrames.get( 0 ).getHeight();
	}

	/**
	 * Generates a max cap for if this {@code Customer} should order a {@code Pizza} based on rules within the method, depending on variables such as hunger.
	 *
	 * @return Max range cap for if this {@code Customer} should order a {@code Pizza} (for use in the form of range 0->return%)
	 */
	private int generateShouldOrderRange()
	{
		int shouldOrderRange = 0;

		if( hunger_level > MAX_HUNGER / 2 )
			shouldOrderRange += 25; //If hunger is greater than 50% than there is a 50% base chance that a pizza will be ordered.

		/* A small % comes from the current hunger_level.
			Examples (Using default 200 for MAX_HUNGER for applying the 50% modifier):
			    hunger_level is 20-29 then there will be a +2% chance of a pizza being ordered (2% total).
			    hunger_level is 50-59 then there will be a +5% chance of a pizza being ordered (5% total).
			    hunger_level is 70-79 then there will be a +7% chance of a pizza being ordered (7% total).
			    hunger_level is 100-109 then there will be a +10% chance of a pizza being ordered (60% total).

			    Note that while these numbers seem small because of the thread sleep rate of 50ms the chance is "increased" by the shear number of "dice-rolls".
		 */
		shouldOrderRange += hunger_level / 10;

		return shouldOrderRange;
	}

	/**
	 * Eat the ordered {@code Pizza}! Call when the {@code Pizza} order this {@code Customer} has placed is ready to be eaten.
	 * <p/>
	 * Causes {@code hunger_level} to reduce by a base of 100 after applying a modifier (i.e. -= 100 * {@code hunger_modifier})
	 */
	private void eatOrder()
	{
		System.out.println( name + ": Eating order" );
		hunger_level -= 100 * hunger_modifier;

		if( hunger_level < 0 )
			hunger_level = 0;

		currentOrder.setHasBeenEaten( true );
		currentOrder = null; //Null the reference to the pizza here, letting it die with GC's next memory sweep
	}

	/**
	 * Called each "live" and makes the customer "tick". This is similar to tick in the Store class, except that Tick isn't applied to threaded objects.
	 * <p/>
	 * For each "live" done the customers hunger will change depending on the conditions, and other checks will be done, such as checking their pizza order.
	 */
	private void live()
	{
		updateDrawing(); //Update the VisualCustomer for each life-cycle

		if( currentOrder != null )
		{
			if( currentOrder.isMade() )
			{
				eatOrder();
			}
		}
		else if( Store.getInstance().canPlaceOrder() )
		{
			hunger_level += hunger_level < MAX_HUNGER ? hungerBuildupRate : 0;

			if( new Random().nextInt( 100 ) < generateShouldOrderRange() )
				placePizzaOrder();
		}
	}

	private void updateDrawing()
	{
		if( currentOrder != null && currentFrame < imageFrames.size() - 1 )
			currentFrame++;
		else
			currentFrame = 0;

		vCustomer.setDrawImage( imageFrames.get( currentFrame ) );
		vCustomer.setHungerLevel( hunger_level );
		vCustomer.setSubText( currentOrder != null ? currentOrder.getPizzaName() : "" );

		vCustomer.repaint(); //After changing the VisualCustomer, tell it to redraw itself to reflect the new changes to its visual output.
	}

	@Override
	public void run()
	{
		//noinspection InfiniteLoopStatement
		while( true )
		{
			live();

			try
			{
				Thread.sleep( 100 );
			}
			catch( Exception e )
			{
				System.out.println( name + ": I woke up early!" );
			}
		}
	}

}
