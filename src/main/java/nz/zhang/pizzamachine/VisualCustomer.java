package nz.zhang.pizzamachine;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * VisualSupplier - visual version of the supplier class, which is controlled by a threaded supplier
 * <p/>
 * Created by G-Rath on 7/04/2015.
 */
public class VisualCustomer extends JPanel
{
	/** Location of where to draw this VisualCustomer's drawImage */
	private Point drawLocation;
	/** Current image to draw at drawLocation */
	private BufferedImage drawImage;
	/** Name of the customer this VisualCustomer represents */
	private String customerName = "none";
	/** Current hunger level of the customer this VisualCustomer represents - for visual output only, not for conditional statements */
	private int hungerLevel = 0;
	/** Text to be drawn under the customer's image */
	private String subText = "";

	public VisualCustomer( Point drawLocation )
	{
		this.drawLocation = drawLocation;
	}

	public void doDrawing( Graphics g )
	{
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawImage( drawImage, drawLocation.x, drawLocation.y, this );
		g2d.setColor( Color.WHITE ); //Make sure that we're drawing in white color, otherwise the text will be hard to read

		g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING,
		                      RenderingHints.VALUE_ANTIALIAS_ON );

		try //Try-Catch is being used instead of an "if != null" condition as for some reason that didn't seem to work as well.
		{
			g2d.drawString( customerName, drawLocation.x + 10, drawLocation.y + 35 ); //We only really need the try-catch for the line below, but no-harm/no-foul
			g2d.drawString( "H:" + hungerLevel, drawLocation.x + drawImage.getWidth() - 10 - getFontMetrics( getFont() ).stringWidth( "H:" + hungerLevel ), drawLocation.y + 35 );
			g2d.drawString( subText, drawLocation.x + 20, drawLocation.y + drawImage.getHeight() - 10 );
		}
		catch( NullPointerException e )
		{
			//Sometimes (rarely) because of a data race with the tick timer the font will be null. This isn't an issue, since we can just not draw the string.
			System.out.println( "FONT WAS NULL" );
		}

		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}

	public Point getDrawLoc()
	{
		return drawLocation;
	}

	public void setDrawImage( BufferedImage drawImage )
	{
		this.drawImage = drawImage;
	}

	public void setCustomerName( String customerName )
	{
		this.customerName = customerName;
	}

	public void setHungerLevel( int hungerLevel )
	{
		this.hungerLevel = hungerLevel;
	}

	public void setSubText( String subText )
	{
		this.subText = subText;
	}
}
