package nz.zhang.pizzamachine;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * {@code PizzaMachine} - Takes orders from {@code Customers}, makes {@code Pizza}s
 * <p/>
 * Created by G-Rath on 27/03/2015.
 */
public class PizzaMachine extends JPanel
{
//	/** Scale being used for images - This is number isn't actually used in doing the scaling, so it must be changed independently of the scaling code. */
//	public static final int SCALE = 2;

	/** The scale amount for {@link IngredientContainer#maxIngredientCount} which is passed as part of the constructor, multiplied by this amount */
	private static final int CONTAINER_MAX_SCALE = 2;

	/** The percent that a {@code IngredientContainer} must have remaining in order to be resupplied regardless of the current order's ingredients */
	private static final float MIN_FULL_PERCENT_CAP = 0.25f;
	/** Controls the number of frames that should pass (while showing the final frame) at the end of an animation before restarting */
	private static final int ANIM_IDLE_LASTFRAME = 3;
	/** Reflects if this {@code PizzaMachine} is making a {@code Pizza} order or not. Useful for avoiding {@code ConcurrentModificationException} */
	public boolean makingOrders = false;

	/** HashMap used for storing {@code IngredientContainer} */
	private HashMap<Ingredient, IngredientContainer> containers = new HashMap<>();

//	private ArrayList<BufferedImage> makingAnimationArray = new ArrayList<>();

	private ArrayList<BufferedImage> imageArray = new ArrayList<>();
	private HashMap<Ingredient, Integer> supplyRequest = new HashMap<>();
	private ArrayList<Pizza> orders = new ArrayList<>();

	/** The current order being processed by this {@code PizzaMachine} */
	private Pizza currentOrder = null;
	/** Current frame being rendered for the visual representation of this {@code PizzaMachine} */
	private int currentFrame = 0;
	/** Current location of this {@code PizzaMachine} visually (frame drawing location) */
	private Point location;
	/** The center of the visual representation of this {@code PizzaMachine}, based off the height and width of the current frame */
	private Point locationCenter;

	public PizzaMachine( Point location )
	{
		this.location = location;

		loadImages();
		setupContainers();
		layoutContainers();
	}

	public void setupContainers()
	{
		//Ingredients (and their max count) are: Garlic (6), Olives (8), Mushrooms (10), Anchovies (6)
		containers.put( Ingredient.GARLIC, new IngredientContainer( 6 * CONTAINER_MAX_SCALE, Ingredient.GARLIC ) );
		containers.put( Ingredient.OLIVES, new IngredientContainer( 8 * CONTAINER_MAX_SCALE, Ingredient.OLIVES ) );
		containers.put( Ingredient.MUSHROOMS, new IngredientContainer( 10 * CONTAINER_MAX_SCALE, Ingredient.MUSHROOMS ) );
		containers.put( Ingredient.ANCHOVIES, new IngredientContainer( 6 * CONTAINER_MAX_SCALE, Ingredient.ANCHOVIES ) );
	}

	private void loadImages()
	{
		for( int i = 0; i < 22 + 1; i++ )
		{
			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;

			try
			{
//				BufferedImage image = ImageIO.read( new File( "sprites/construction-yard-td/fact " + imageNumberString + ".png" ) );

				String filePath = "/sprites/construction-yard-td/fact " + imageNumberString + ".png";
				InputStream in = getClass().getResourceAsStream( filePath );
				BufferedImage image = ImageIO.read( in );

				imageArray.add( Store.scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
//				imageArray.add( ImageIO.read( new File( "sprites/nuk2 " + imageNumberString + ".png" ) ) );
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}

		locationCenter = new Point( location.x + imageArray.get( 0 ).getWidth() / 2, location.y + imageArray.get( 0 ).getHeight() / 2 );
	}

	public void layoutContainers()
	{
		//containerLocation.setSize( containerGarlic.getCurrentFrame().getWidth() + 50, containerGarlic.getCurrentFrame().getWidth() + 50 );
		Point containerLocation = new Point( 48, 48 );

		System.out.println( containers.get( Ingredient.GARLIC ).getCurrentFrame().getWidth() );

		containers.get( Ingredient.GARLIC ).setLocation( new Point( containerLocation ) );
		containerLocation.x += containers.get( Ingredient.GARLIC ).getCurrentFrame().getWidth() + 50;

		containers.get( Ingredient.OLIVES ).setLocation( new Point( containerLocation ) );
		containerLocation.x += containers.get( Ingredient.OLIVES ).getCurrentFrame().getWidth() + 50;

		containers.get( Ingredient.MUSHROOMS ).setLocation( new Point( containerLocation ) );
		containerLocation.x += containers.get( Ingredient.MUSHROOMS ).getCurrentFrame().getWidth() + 50;

		containers.get( Ingredient.ANCHOVIES ).setLocation( new Point( containerLocation ) );

	}

	/**
	 * Returns a reference of {@code supplyRequest} (reference due to map shallow-copy)
	 *
	 * @return {@code supplyRequest} shallow-copy reference. Should be deep-copied by using HashMap's copy-constructor
	 */
	public HashMap<Ingredient, Integer> getSupplyRequestMap()
	{
		return supplyRequest; //Return by reference
	}

	public void placeNewSupplyRequest( Ingredient ingredient )
	{
		if( supplyRequest.get( ingredient ) == null )
		{
			System.out.println( "Placing new supply request for " + ingredient );
			supplyRequest.put( ingredient, 0 );

			containers.get( ingredient ).setWaitingForResupply( true );
		}
	}

	public void updateSupplyRequest( Ingredient ingredient, int newRequestState )
	{
		supplyRequest.put( ingredient, newRequestState );
		cleanSupplyRequests();
	}

	public void cleanSupplyRequests()
	{
		//Loop though the supplyRequests HashMap and check each requests state
		for( Ingredient ingredient : Ingredient.values() )
		{
			//State -1 means the request has been finished, so remove it.
			if( supplyRequest != null && supplyRequest.get( ingredient ) != null && supplyRequest.get( ingredient ) == -1 )
				supplyRequest.remove( ingredient );
		}
	}

	/**
	 * Checks if an {@code IngredientContainer} should be resupplied because its under the min % full amount.
	 * This is called when there are no orders to make, as a means of resupplying containers that are low.
	 *
	 * @param container the {@link IngredientContainer} to check
	 *
	 * @return {@code true} if the {@code IngredientContainer} is below the min full %, otherwise {@code false}.
	 */
	public static boolean shouldResupply( IngredientContainer container )
	{
		return container.getMaxIngredientCount() - container.getCurrentIngredientCount() == container.getMaxIngredientCount() * MIN_FULL_PERCENT_CAP;
	}

	public IngredientContainer getIngredientContainer( Ingredient ingredient )
	{
		return containers.get( ingredient );
	}

	public void doDrawing( Graphics g )
	{
		g.setFont( Store.monoFont );

		Graphics2D g2d = (Graphics2D) g;

		BufferedImage image = imageArray.get( currentFrame );

		g2d.drawImage( image, location.x, location.y, this );

		if( currentOrder != null && currentOrder.getOrdererLocation() != null )
		{
			g.setColor( Color.GREEN );

			//Use location center, and offset it a little so the line is from the "pizza" that's part of the animation
			g.drawLine( locationCenter.x - 10, locationCenter.y + 30, currentOrder.getOrdererLocation().x, currentOrder.getOrdererLocation().y );
			g.drawRect( currentOrder.getOrdererLocation().x - 10, currentOrder.getOrdererLocation().y - 10, 20, 20 );
			g.setColor( Color.WHITE ); //Set the color back to white for whatever renders next
		}

		for( IngredientContainer container : containers.values() )
		{
			container.paint( g2d, this );
		}

		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	private void checkAllContainersIfResupply()
	{
		for( IngredientContainer container : containers.values() )
		{
			if( shouldResupply( container ) )
				placeNewSupplyRequest( container.getIngredient() );
		}
	}

	/**
	 * Check all the current {@code Pizza} orders in {@code orders} to see if any can be made.
	 * If an order can't be made at the present time, then the {@code PizzaMachine} place a resupply request for all {@code Ingredient}s in the given order
	 * that require resupplying before the order can be made.
	 */
	public void checkOrders()
	{
		if( orders.isEmpty() )
		{
			checkAllContainersIfResupply();
		}
		else
		{
			makingOrders = true;

			for( Pizza order : new ArrayList<>( orders ) )
			{
				if( canMakeOrder( order ) ) //Check if we can make this pizza order...
				{
					if( currentOrder == null ) //We can only do one order at a time
					{
						makeOrder( order );     //...and if we can, make it...
						currentOrder = order;   //Set the current order to the order we're making
						currentFrame = 3;       //Set the currentFrame to 3, the starting frame of the make animation.
					}
				}
				else    //...else check what supplies we're lacking, and submit an order for them
				{
					//System.out.println( "Can't make order" );
					orderResupplyForOrder( order );
				}
			}
		}

		orders.remove( currentOrder ); //Remove the currentOrder from the orders array. If currentOrder is null this will have no effect.
		makingOrders = false; //We've trying to make the orders
	}

	/**
	 * Makes the given {@code Pizza} order, expending the {@code Ingredient}s by decreasing from the respective {@code IngredientContainer}
	 *
	 * @param order The {@code Pizza} order to make
	 */
	public void makeOrder( Pizza order )
	{
		HashMap<Ingredient, Integer> ingredients = new HashMap<>( order.getIngredients() );
		String ingredientsUsed = "";

		for( Map.Entry<Ingredient, Integer> entry : ingredients.entrySet() )
		{
			expendIngredient( entry.getKey(), entry.getValue() ); //Expend the current ingredient
			ingredientsUsed += entry.getKey() + ", " + entry.getValue() + " ";
		}

		Store.getInstance().addTextToPrinter( "Machine: order for " + order.getOrderer() + ", " + order.getPizzaName() );
		System.out.println( "Machine: order made - " + order.getPizzaName() + ", ingredients used: " + ingredientsUsed );
	}

	/**
	 * Expends {@code Ingredient}s by taking the given {@code Ingredient}s amount away from the total in the respective {@code IngredientContainer}
	 *
	 * @param ingredient The {@code Ingredient}s to expend
	 * @param amount     How much of the given {@code Ingredient}s to be expend
	 */
	public void expendIngredient( Ingredient ingredient, int amount )
	{
		System.out.println( "\t" + "Used ingredients: " + ingredient + ", " + amount );
		containers.get( ingredient ).useIngredient( amount );
	}

	public void orderResupplyForOrder( Pizza order )
	{
		HashMap<Ingredient, Integer> ingredients = new HashMap<>( order.getIngredients() );

		Iterator it = ingredients.entrySet().iterator();
		while( it.hasNext() )
		{
			Map.Entry pair = (Map.Entry) it.next();

			//If the container doesn't meet the requirements given for this order, submit a new supply request.
			//if( !checkContainerMeetsRequirement( (Ingredient) pair.getKey(), (int) pair.getValue() ) )
			//containers.get( ingredient ).getCurrentIngredientCount()
			if( !( containers.get( pair.getKey() ).getCurrentIngredientCount() < (int) pair.getValue() ) )
				placeNewSupplyRequest( (Ingredient) pair.getKey() );

			it.remove(); // avoids a ConcurrentModificationException
		}
	}

	/**
	 * Compares the amount of {@code Ingredient}s in each {@code IngredientContainer} to the required {@code Ingredient}s for a {@code Pizza} order.
	 *
	 * @param order Pizza order who's {@code Ingredient}s to check
	 *
	 * @return {@code true} if the {@code IngredientContainer}s have enough {@code Ingredient}s to make the give {@code Pizza}. Otherwise {@code false}
	 */
	public boolean canMakeOrder( Pizza order )
	{
		HashMap<Ingredient, Integer> ingredients = new HashMap<>( order.getIngredients() );

		Iterator it = ingredients.entrySet().iterator();
		while( it.hasNext() )
		{
			Map.Entry pair = (Map.Entry) it.next();

			//If the container doesn't meet the requirements given for this order, return false right away, since we can't make the order.
			if( !( containers.get( pair.getKey() ).getCurrentIngredientCount() < (int) pair.getValue() ) )
			{
				placeNewSupplyRequest( (Ingredient) pair.getKey() );
				return false;
			}

			it.remove(); // avoids a ConcurrentModificationException
		}

		return true; //If we've looped though the whole map and all requirements have been meet, then we can make the pizza order.
	}

	/**
	 * Place an order for a {@code Pizza} using the given {@code Pizza} instance
	 *
	 * @param pizza an instance of {@link Pizza} to be added to the {@code orders} array as a new {@code Pizza} order.
	 */
	public void placeOrder( Pizza pizza )
	{
		orders.add( pizza );
	}

	/**
	 * Checks if it's considered "safe" for to place an order (via {@code placeOrder}).
	 * This should be checked to avoid a {@code ConcurrentModificationException}.
	 *
	 * @return {@code true} if its safe to place an order, {@code false} otherwise
	 */
	public boolean isSafeToPlaceOrder()
	{
		return !makingOrders;
	}

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}

	public void tick()
	{
		if( currentOrder != null && currentOrder.hasBeenEaten() )
			currentOrder = null; //If the customer has eaten the pizza, set the order to null for GC to take care of.

		checkOrders();

		if( currentOrder != null )
		{
			if( currentFrame == 22 )
				currentOrder.setIsMade( true ); //System.out.println( "Machine: order is made" );
			else if( currentOrder.isMade() )
				currentFrame = 22;
			else
				currentFrame += ( currentFrame + 4 > imageArray.size() - 1 ? 1 : 4 );
		}
		else if( currentFrame < ANIM_IDLE_LASTFRAME )
			currentFrame++;
		else
			currentFrame = 0;

		tickContainers();

		repaint();
	}

	public void tickContainers()
	{
		for( IngredientContainer container : containers.values() )
		{
			container.tick();
		}
	}
}