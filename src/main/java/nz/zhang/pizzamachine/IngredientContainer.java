package nz.zhang.pizzamachine;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

/**
 * IngredientContainer class - Originally named just "Container" but Java got pissy as its the same as java.awt.container, and so the IDE was getting confused.
 * <p/>
 * Created by G-Rath on 27/03/2015.
 */
public class IngredientContainer
{
	/** This containers location for drawing */
	private Point location;
	/** The type of {@code Ingredient} this {@code IngredientContainer} holds */
	private Ingredient ingredientType = Ingredient.NONE;

	/** The max possible value of {@link IngredientContainer#currentIngredientCount}. */
	private int maxIngredientCount;

	/**
	 * The amount of {@code ingredientType} this {@code IngredientContainer} currently holds.
	 * This value can't be greater than {@link IngredientContainer#maxIngredientCount}
	 */
	private int currentIngredientCount = 0;

	private ArrayList<BufferedImage> frameArray = new ArrayList<>();
	private int currentImageFrame = 0;
	/** Indicates if this {@code IngredientContainer} is waiting to be resupplied. */
	private boolean isWaitingForResupply = false;

	/**
	 * Creates a new container that holds up to {@code maxIngredientCount} amount of {@code ingredientType}
	 *
	 * @param maxIngredientCount the highest possible value that {@code currentIngredientCount} can be.
	 * @param ingredientType     the type of {@code Ingredient} this container holds.
	 */
	public IngredientContainer( int maxIngredientCount, Ingredient ingredientType )
	{
		this.maxIngredientCount = maxIngredientCount;
		this.ingredientType = ingredientType;

		currentIngredientCount = maxIngredientCount;

		loadImages();
	}

	public Point getLocation()
	{
		return location;
	}

	public void setLocation( Point location )
	{
		this.location = location;
	}

	private void loadImages()
	{
		//Slightly better way of doing it since they all use the same container images
		frameArray = Store.getInstance().getContainerImages();

		//region old code for loading container images
		//
//		for( int i = 0; i < 6 + 1; i++ )
//		{
//			String imageNumberString = ( i <= 999 ? ( i <= 99 ? ( i <= 9 ? "000" : "00" ) : "0" ) : "" ) + i;
//
//			try
//			{
//				//System.out.println( imageNumberString );
//				BufferedImage image = ImageIO.read( new File( "sprites/hpad/hpad " + imageNumberString + ".png" ) );
//
//				frameArray.add( scale( image, image.getType(), image.getWidth(), image.getHeight(), 2, 2 ) );
//				//frameArray.add( image );
//			}
//			catch( IOException e )
//			{
//				e.printStackTrace();
//			}
//		}
		//endregion
	}

	public Ingredient getIngredient()
	{
		return ingredientType;
	}

	@SuppressWarnings( "unused" )
	public void setIngredientType( Ingredient ingredientType )
	{
		this.ingredientType = ingredientType;
	}

	public void refillContainer()
	{
		setCurrentIngredientCount( maxIngredientCount );
	}

	public int getCurrentIngredientCount()
	{
		return currentIngredientCount;
	}

	public void setCurrentIngredientCount( int currentIngredientCount )
	{
		this.currentIngredientCount = currentIngredientCount;
	}

	@SuppressWarnings( "unused" )
	public int getMaxIngredientCount()
	{
		return maxIngredientCount;
	}

	@SuppressWarnings( "unused" )
	public BufferedImage getFrameAt( int frame ) throws IndexOutOfBoundsException
	{
		return frameArray.get( frame );
	}

	public BufferedImage getCurrentFrame()
	{
		return frameArray.get( currentImageFrame );
	}

	public void tick()
	{
		if( isWaitingForResupply && currentImageFrame < frameArray.size() - 1 )
			currentImageFrame++;
		else
			currentImageFrame = 0;
	}

	public void paint( Graphics2D g2d, ImageObserver container )
	{
		BufferedImage image = frameArray.get( currentImageFrame );

		//System.out.println( StoreManager.ingredientAsString( ingredientType ) + ": " + location);
		g2d.drawImage( image, location.x, location.y, container );

		g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING,
		                      RenderingHints.VALUE_ANTIALIAS_ON );

		g2d.drawString( "C: " + currentIngredientCount + " / " + maxIngredientCount, location.x, location.y + image.getHeight() );
	}

	public void useIngredient( int amount )
	{
		System.out.println( "Ingredient used" );
		currentIngredientCount = ( ( currentIngredientCount - amount ) >= 0 ) ? ( currentIngredientCount - amount ) : 0;
	}

	@SuppressWarnings( "unused" )
	public int getCurrentImageFrame()
	{
		return currentImageFrame;
	}

	@SuppressWarnings( "unused" )
	public void setCurrentImageFrame( int currentImageFrame )
	{
		this.currentImageFrame = currentImageFrame;
	}

	@SuppressWarnings( "unused" )
	public boolean isWaitingForResupply()
	{
		return isWaitingForResupply;
	}

	public void setWaitingForResupply( boolean waitingForResupply )
	{
		this.isWaitingForResupply = waitingForResupply;
	}
}
