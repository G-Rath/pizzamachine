package nz.zhang.pizzamachine;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.text.AttributedString;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * TextPrinter
 * Created by G-Rath on 13/04/2015.
 */
public class TextPrinter extends JPanel
{
	private ArrayList<String> textLines = new ArrayList<>();

	private Point location;

	public TextPrinter( Point location )
	{
		this.location = location;
	}

	/**
	 * Gets the current system time in the date format "HH:mm:ss" ( Hour, minutes, seconds)
	 *
	 * @return - Current time as string
	 */
	public static String getCurrentTime()
	{
		return new SimpleDateFormat( "HH:mm:ss" ).format( Calendar.getInstance().getTime() );
	}

	public void addText( String textToAdd )
	{
		textLines.add( "[" + getCurrentTime() + "] " + textToAdd );

		if( textLines.size() > 10 ) //Keep the number of lines in the text output below 10 for performance reasons.
			textLines.remove( 0 );
	}

	public void doDrawing( Graphics g )
	{
		g.setFont( Store.monoFont );

		Graphics2D g2d = (Graphics2D) g;

		g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );

		//g2d.drawString( text, location.width, location.height );
		g2d.setColor( Color.WHITE );

		//order for <color: green> joey </color>

		Point pen = new Point( location.x, location.y );
		FontRenderContext frc = g2d.getFontRenderContext();

		for( int i = textLines.size() - 1; i >= 0; i-- )
		{
			String line = textLines.get( i );
			LineBreakMeasurer measurer = new LineBreakMeasurer( new AttributedString( line ).getIterator(), frc );
			float wrappingWidth = getSize().width - 15;

			while( measurer.getPosition() < line.length() )
			{
				TextLayout layout = measurer.nextLayout( wrappingWidth );

				pen.y -= ( layout.getAscent() );
				float dx = layout.isLeftToRight() ? 0 : ( wrappingWidth - layout.getAdvance() );

				layout.draw( g2d, pen.x + dx, pen.y );
				pen.y -= layout.getDescent() + layout.getLeading();
			}
		}

//		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
//		int screenX = gd.getDisplayMode().getWidth();
//		int screenY = gd.getDisplayMode().getHeight();
//		g2d.setColor( Color.BLACK );
//		g2d.fillRect( location.x, 0, screenX, location.y - 210 );
		//BufferedImage image = imageArray.get( currentImage );

		//g2d.drawImage( image, location.width, location.height, this );

//		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
//		int screenX = gd.getDisplayMode().getWidth();
//		int screenY = gd.getDisplayMode().getHeight();
//
//		setBounds( 0, 0, screenX, screenY );
		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}
}