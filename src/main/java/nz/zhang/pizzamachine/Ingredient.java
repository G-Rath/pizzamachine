package nz.zhang.pizzamachine;

/**
 * {@code Ingredient} enum - Represents the possible {@code Ingredient}s that can be used in a {@code Pizza}
 * <p/>
 * Created by G-Rath on 7/04/2015.
 * <p/>
 */

public enum Ingredient
{
	NONE, GARLIC, OLIVES, MUSHROOMS, ANCHOVIES;

	/**
	 * Compares a given string to the {@code Ingredient}s in the {@code Ingredient} enum, comparing to their direct names (i.e GARLIC is "garlic" in string)
	 * Overload method for the "ignoreCase" parameter, which defaults to true without having to worry about the second parameter.
	 *
	 * @param ingredient The {@code Ingredient} as a string. this string is trimmed of leading and trailing spaces before its switched on.
	 *
	 * @return The enum value of the {@code Ingredient} from the string given
	 */
	public static Ingredient fromString( String ingredient )
	{
		return fromString( ingredient, true );
	}

	/**
	 * Compares a given string to the ingredients in the Ingredient enum, comparing to their direct names (i.e GARLIC is "garlic" in string)
	 * This direct version should only be called if we care about case. Other-wise call the overloaded version of this function which takes only one parameter.
	 *
	 * @param ingredient The {@code Ingredient} as a string. this string is trimmed of leading and trailing spaces before its switched on.
	 * @param ignoreCase Should we ignore cases? If true the string will be converted to lower-case.
	 *
	 * @return The enum value of the {@code Ingredient} from the string given
	 */
	public static Ingredient fromString( String ingredient, boolean ignoreCase )
	{
		if( ignoreCase ) //Convert to lower-case if we're ignoring cases
			ingredient = ingredient.toLowerCase();

		switch( ingredient.trim() ) //Not  wanting to piss around with leading and trailing spaces, trim without care.
		{
			case "none":
				return NONE;

			case "garlic":
				return GARLIC;

			case "olives":
				return OLIVES;

			case "mushrooms":
				return MUSHROOMS;

			case "anchovies":
				return ANCHOVIES;

			default:
				return null;
		}
	}

	@Override
	public String toString()
	{
		switch( this )
		{
			case NONE:
				return "none";

			case GARLIC:
				return "garlic";

			case OLIVES:
				return "olives";

			case MUSHROOMS:
				return "mushrooms";

			case ANCHOVIES:
				return "anchovies";

			default:
				return super.toString();
		}
	}
}