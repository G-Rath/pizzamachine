package nz.zhang.pizzamachine;

import javax.swing.*;

/**
 * StoreManager
 * Created by G-Rath on 27/03/2015.
 */
public class StoreManager
{
	public static void main( String[] args )
	{
		SwingUtilities.invokeLater( new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
				}
				catch( ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e )
				{
					e.printStackTrace();
				}

				Store store = Store.getInstance();
				store.setVisible( true );
				store.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );

				store.setupStore();
			}
		} );
	}
}