package nz.zhang.pizzamachine;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Supplier - Resupplies containers for the PizzaMachine by flying
 * <p/>
 * Created by G-Rath on 7/04/2015.
 */
public class Supplier implements Runnable
{
	/** Offset for doing manual adjustments to the landing location, as some sprites have "issues" */
	private final Point landingLocationOffset = new Point( 0, -20 );

	@SuppressWarnings( "FieldCanBeLocal" )
	private final int frame_DirectNorth = 0;
	@SuppressWarnings( "unused" )
	private final int frame_DirectWest = 9;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int frame_DirectSouth = 16;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int frame_LandingDirection = 10;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int waitTime = 10;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int landingTarget_HeightOffset = 5;

	public ArrayList<BufferedImage> flyingFrames = new ArrayList<>();
	public ArrayList<BufferedImage> landingFrames = new ArrayList<>();

	private int waitingCounter = 0;
	/** Current direction this supplier is facing based on a integer compass direction, which relates to the index location in the frames array */
	private int frame_CurrentDirection = 0;
	/** The height this supplier should fly at */
	private int flyingHeight = 10;
	/** The target container that this supplier is trying to resupply */
	private IngredientContainer supplyTarget;
	/** Ingredient that this Supplier is carrying */
	private Ingredient ingredient;
	/** Target for this Supplier to land at */
	private Point landingTarget;
	/** Current location of this Supplier */
	private Point location;
	/** currentHeight of this Supplier (since its flying) - We start in flight so hence = flyingHeight */
	private int currentHeight = flyingHeight;
	/** Movement direction of this Supplier as a string, relating to the points on a compass */
	private String moveDirection = "south";
	/** VisualSupplier that this Supplier controls, and uses to give itself a visual representation */
	private VisualSupplier vSupplier;
	/** Has this Supplier done its job (and hence is just flying away) yet? */
	private boolean jobDone = false;

	Supplier( VisualSupplier vS, IngredientContainer supplyTarget, boolean compareImages )
	{
		vSupplier = vS;
		System.out.println( "Supplier on route!" );

		this.supplyTarget = supplyTarget;
		ingredient = supplyTarget.getIngredient();

		configSupplier( compareImages );
		vSupplier.setDrawLocation( location );
	}

	/**
	 * Checks if the given point location is within the screen bounds.
	 * <p/>
	 * This can be used to check if a visual component is visible by the user at the given point
	 *
	 * @param location The point to check
	 *
	 * @return True if on screen, false otherwise
	 */
	public static boolean isLocationInScreenBounds( Point location )
	{
		GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] graphicsDevices = graphicsEnvironment.getScreenDevices();
		Rectangle graphConfigBounds = new Rectangle();

		for( GraphicsDevice graphicsDevice : graphicsDevices ) //For each graphical device...
		{

			graphConfigBounds.setRect( graphicsDevice.getDefaultConfiguration().getBounds() ); //Get device bounds
			graphConfigBounds.setRect( graphConfigBounds.x, graphConfigBounds.y, graphConfigBounds.width, graphConfigBounds.height );

			if( graphConfigBounds.contains( location.x, location.y ) )
				return true; // The location is in this screen graphics.
		}

		return false; // We could not find a device that contains the given point.
	}

	private void configSupplier( boolean compareImages )
	{
		getImagesFromStore( compareImages );
		location = getStartingPointForTarget( supplyTarget );
		landingTarget = getLandingPointForTarget( supplyTarget );
	}

	/**
	 * Calculates the {@code Point} to start at visually for "flying" to the given {@code IngredientContainer}.
	 * <p/>
	 * This is calculated by finding the center {@code Point} of the {@code IngredientContainer} the {@code Supplier} is trying to land on,
	 * and subtracting 100 from the {@code Point.y} value.
	 *
	 * @param supplyTarget the {@code IngredientContainer} this {@code Supplier} is flying to
	 *
	 * @return the starting {@link Point} that this {@code Supplier} should start at visually.
	 */
	private Point getStartingPointForTarget( IngredientContainer supplyTarget )
	{
		int x = (int) ( supplyTarget.getLocation().getX() + supplyTarget.getCurrentFrame().getWidth() / 2 );
		int y = (int) ( supplyTarget.getLocation().getY() + supplyTarget.getCurrentFrame().getHeight() / 2 );

		return location = new Point( x, y - 100 );
	}

	/**
	 * Calculates the {@code Point} that this {@code Supplier} must reach in order to be considered as having "landed", and so can re-supply.
	 * <p/>
	 * This is calculated by finding the center {@code Point} of the {@code IngredientContainer} the {@code Supplier} is trying to land on,
	 * and adding the value of {@code landingTarget_HeightOffset} to the {@code Point.y} value.
	 *
	 * @param supplyTarget the {@code IngredientContainer} this {@code Supplier} wanting to land on.
	 *
	 * @return the {@link Point} that this {@code Supplier} should be considered as having "landed" successfully.
	 */
	private Point getLandingPointForTarget( IngredientContainer supplyTarget )
	{
		int x = (int) ( supplyTarget.getLocation().getX() + ( supplyTarget.getCurrentFrame().getWidth() / 2 ) + landingLocationOffset.getX() );
		int y = (int) ( supplyTarget.getLocation().getY() + ( supplyTarget.getCurrentFrame().getHeight() / 2 ) + landingLocationOffset.getY() );

		return new Point( x, y + landingTarget_HeightOffset );
	}

	/**
	 * "resupplies" the target {@code IngredientContainer} by calling {@link IngredientContainer#refillContainer()}.
	 * {@link Store#finishedResupply(Ingredient)} is then called so that the {@code Store} knows
	 * is called telling to
	 */
	private void resupplyContainer()
	{
		supplyTarget.refillContainer(); //resupply the container
		Store.getInstance().finishedResupply( ingredient ); //Tell the store that we have finished resupplying the container
		moveDirection = "wait"; //Time to fly away!
		jobDone = true;
	}

	/**
	 * Gets the location of this supplier's image - This is important since our locations are center points, but images are rendered by the top-left corner.
	 *
	 * @return our current location minus half the height and width of our current image
	 */
	private Point getImageCenteredLocation()
	{
		return new Point( location.x - ( getCorrectFrame().getWidth() / 2 ), location.y - ( getCorrectFrame().getHeight() / 2 ) );
	}

	private void getImagesFromStore( boolean useAlternativeHeliImages )
	{
		if( useAlternativeHeliImages )
		{
			landingFrames = Store.getInstance().getSupplierSpriteArray( "heli-landing" );
			flyingFrames = Store.getInstance().getSupplierSpriteArray( "heli-flying" );
		}
		else
		{
			landingFrames = Store.getInstance().getSupplierSpriteArray( "orca-landing" );
			flyingFrames = Store.getInstance().getSupplierSpriteArray( "orca-flying" );
		}
	}

	private void tick()
	{
		updateDrawing(); //Update the VisualSuppliers details for drawing, such as its current image and location
		calcMovement(); //Then calcMovement a bit
	}

	@Override
	public void run()
	{
		while( !jobDone || isLocationInScreenBounds( location ) )
		{
			tick();

			try
			{
				Thread.sleep( 25 );
			}
			catch( Exception e )
			{
				System.out.println( "Supplier: Someone woke me up early!" );
			}
		}

		Store.getInstance().removeVSupplier( vSupplier );
		vSupplier = null;
	}

	public void updateDrawing()
	{
		BufferedImage image;
		image = getCorrectFrame();

		Point actualLocation = getLocationWithFlyingHeight( getImageCenteredLocation() );

		vSupplier.setDrawLocation( actualLocation );    //Update the VisualSupplier's location where its currently drawing its image
		vSupplier.setDrawImage( image );                //Update the VisualSupplier's image that's currently being drawn

		vSupplier.repaint(); //Repaint ourselves
	}

	public BufferedImage getCorrectFrame()
	{
		switch( moveDirection )
		{
			default:
			case "hover": //Mainly here for fun - this program shouldn't ever likely need a supplier to "hover"
			case "north":
			case "south":
				return flyingFrames.get( frame_CurrentDirection );
			case "up":
			case "down":
			case "wait":
				return landingFrames.get( frame_CurrentDirection );
		}
	}

	public Point getLocationWithFlyingHeight( Point loc )
	{
		return new Point( loc.x, loc.y - currentHeight );
	}

	public void calcMovement()
	{
		int x = location.x;
		int y = location.y;

		switch( moveDirection )
		{
			case "north":
				y -= 5;//unitFrame_North.getHeight() * 0.25;

				if( frame_CurrentDirection != frame_DirectNorth )
					frame_CurrentDirection--;

				location.setLocation( x, y );
				break;

			case "south":
				frame_CurrentDirection = frame_DirectSouth;

				y += 5;//unitFrame_North.getHeight() * 0.25;

				location.setLocation( x, y );

				if( location.getY() > landingTarget.getY() )
				{
					location.setLocation( landingTarget.x, landingTarget.y ); //We've likely overshot by a little, so we'll cheat to get back into place
					moveDirection = "down";
				}
				break;

			case "up":
				if( currentHeight >= flyingHeight )
				{
					moveDirection = "north";
				}
				else
				{
					currentHeight++;
				}
				break;

			case "down":
				if( currentHeight <= 0 )
				{
					currentHeight = 0; //Don't change calcMovement direction here - instead call resupply which will hand calcMovement direction changes
					resupplyContainer();
				}
				else
				{
					currentHeight--;

					if( frame_CurrentDirection != frame_LandingDirection )
						frame_CurrentDirection--;
				}
				break;

			case "wait":
				if( waitingCounter < waitTime )
					waitingCounter++;
				else
					moveDirection = "up";
				break;

			default:
				break; //do nothing
		}
	}
}
