package nz.zhang.pizzamachine;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * VisualSupplier - visual representation of the supplier class, which is controlled by a threaded supplier
 * <p/>
 * Created by G-Rath on 7/04/2015.
 */
public class VisualSupplier extends JPanel
{
	private Point drawLocation;

	private BufferedImage drawImage;

	VisualSupplier()
	{

	}

	public void setDrawImage( BufferedImage drawImage )
	{
		this.drawImage = drawImage;
	}

	public void doDrawing( Graphics g )
	{
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawImage( drawImage, drawLocation.x, drawLocation.y, this );

		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}

	public void setDrawLocation( Point drawLocation )
	{
		this.drawLocation = drawLocation;
	}
}
