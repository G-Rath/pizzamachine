# PIZZA MACHINE #

This program is a multi-threaded application written in Java. It was originally set as an assignment as part of my University degree.

The assignment called for students to design a "pizza-machine" which would have a number of components that would operate on their own threads, with the goal being to have students learn how to manage multithreading in Java, and some of the issues that occur with multithreading (and how to manage them) such as race-conditions and deadlock situations. 

While the assignment had no requirement for a visual representation of the machine (i.e you could just print operation information to the console) I liked the idea of creating a fully autonomous program that you could actually watch as it "worked". 

For this project I used a couple of sprites ripped from the old Command & Conquer games, namely Tiberium Dawn and Red Alert 1. The program was made up a number of key components that ran on their own thread, with each component being made up of two classes - the class which represented the actual component running on its own thread, and a "visual" class for that component. Whenever a new version of a component was instanced, a new version of the components visual class would also be instanced and passed to the Store class to be rendered. The programs core components are:

**Pizza-Machine**

* Represented by a mobile construction yard

**Suppliers** 

* Represented by helicopters, this component handles restocking a container with an ingredient. There are two different heli sprites, with the supplier class picking one at random to use as part of its constructor.
* A supplier would be "called-in" by the pizza-machine when it wanted a container resupplied. This would cause a new supplier instant to be created (on a new thread), which in turn would "fly" to the container, "land" (with the container's stock not being updated until the supplier considered itself to have landed), and then take off and fly away.
* Once outside the render boundary of the GUI window, the supplier would have itself de-referenced and the thread released so it would be deleted by the Java Garbage Collection process.

**Customers**

* Represented by service deports, this component handles ordering pizzas from the pizza-machine and consuming them. When waiting for a pizza, the customer plays a flashing animation.
* A hunger variable is used to work out when a customer actually orders a pizza - customers have a base % chance of ordering a pizza, which is increased the greater their hunger level.
* The type of pizza a customer orders is taken randomly from an array. Orders are a JSON string that contains the name of the pizza, and the ingredients of the pizza (along with quantities), which are stored in a external file that's read when the program first starts.

Cover:
Reading possible pizza orders from file (for customers)
Store: creates the pizza machine and holds references. creates customers.
Containers: holds ingredients, resupplied by "suppliers". has current/max